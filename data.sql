CREATE SCHEMA target;

CREATE TABLE target.company (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL UNIQUE,
  address VARCHAR(255) NOT NULL
);

CREATE TABLE target.users (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL UNIQUE,
  company_id SERIAL,
  active BOOLEAN NOT NULL,
  FOREIGN KEY (company_id) REFERENCES target.company(id)
);

INSERT INTO target.company (name, address)
values ('株式会社プヨプヨケイタロス', '東京都新宿区なんとかビル');
INSERT INTO target.company (name, address)
values ('（株）プニミンズ', '東京都港区ほんわかビル');
INSERT INTO target.company (name, address)
values ('ボヨヨール株式会社', '東京都品川区のんびりビル');

INSERT INTO target.users (name, email, company_id, active) 
values ('puyo-keitaro', 'keitaro@puyopuyo.com', 1, true);
INSERT INTO target.users (name, email, company_id, active) 
values ('puni-wadamin', 'wadamin@punipuni.com', 2, true);
INSERT INTO target.users (name, email, company_id, active) 
values ('boyo-wadajin', 'wadajin@boyoboyo.com', 3, true);
INSERT INTO target.users (name, email, company_id, active) 
values ('puyo-wadashi', 'wadashi@puyopuyo.com', 1, true);
INSERT INTO target.users (name, email, company_id, active) 
values ('puyo-yametan', 'yametan@puyopuyo.com', 1, false);
INSERT INTO target.users (name, email, company_id, active) 
values ('boyo-yametan', 'yametan@boyoboyo.com', 3, false);
