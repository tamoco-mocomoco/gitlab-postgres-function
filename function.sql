CREATE OR REPLACE FUNCTION target.get_user_list ()
   RETURNS TABLE (
      user_name VARCHAR,
      user_email VARCHAR,
      company_name VARCHAR
   )
AS $$
BEGIN
   RETURN QUERY SELECT
      u.name,
      u.email,
      c.name
   FROM target.users u
   INNER JOIN target.company c
   ON u.company_id = c.id
   WHERE
      u.active is true;   
END; $$
 
LANGUAGE 'plpgsql';